import XCTest
@testable import LibUI

final class LibUITests: XCTestCase {
    
    func testUIColor() throws {
        
        XCTAssertEqual(UIColor.ex.color(hex: "#FFFFFF"), UIColor(red: 1, green: 1, blue: 1, alpha: 1))
    }
}
