var app = {

  version: "0.0.1",

  data: {
    /**
     * 回调缓存
     */
    callbackHandlers: {},
  },

  /**
   * 生成uuid
   *
   * @returns String uuid
   */
  uuid: function () {
    var d = Date.now();
    if (
      typeof performance !== "undefined" &&
      typeof performance.now === "function"
    ) {
      d += performance.now(); //use high-precision timer if available
    }
    return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(
      /[xy]/g,
      function (c) {
        var r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === "x" ? r : (r & 0x3) | 0x8).toString(16);
      }
    );
  },

  /**
   * 判断客户端平台。
   *
   * @returns 返回类型包括iOS、Android、PC。
   */
  clientType: function () {
    let client = "";
    if (/(iPhone|iPad|iPod|iOS)/i.test(navigator.userAgent)) {
      //判断iPhone|iPad|iPod|iOS
      client = "iOS";
    } else if (/(Android)/i.test(navigator.userAgent)) {
      //判断Android
      client = "Android";
    } else {
      client = "PC";
    }
    return client;
  },

  /**
   * 调用客户端提供的方法，不同平台使用不同的方法。
   *
   * @param {String} messageId 方法（消息）id。
   * @param {Object} params 参数。
   */
  postMessage: function (messageId, params) {
    let messages = {
      id: messageId,
      params: params,
    };

    switch (this.clientType()) {
      case "iOS":
        window.webkit.messageHandlers["app"].postMessage(messages);
        break;

      case "Android":
        window.messageHandlers["onMessage"](JSON.stringify(messages));
        break;
    }
  },

  /**
   * 通用成功回调
   *
   * @param {String} uuid
   * @param {Int} code
   * @param {String} message
   */
  callbackSuccess: function (uuid, code, message, data) {
    if (this.data.callbackHandlers[uuid] == null) {
      return;
    }
    if (this.data.callbackHandlers[uuid].success != null) {
      this.data.callbackHandlers[uuid].success({ code, message, data });
    }
  },

  /**
   * 通用失败回调
   *
   * @param {Sting} uuid
   * @param {Int} code
   * @param {String} message
   */
  callbackFail: function (uuid, code, message, data) {
    if (this.data.callbackHandlers[uuid] == null) {
      return;
    }
    if (this.data.callbackHandlers[uuid].fail != null) {
      this.data.callbackHandlers[uuid].fail({ code, message, data });
    }
  },

  /**
   * 通用完成回调
   *
   * @param {String} uuid
   * @param {Int} code
   * @param {String} message
   */
  callbackComplete: function (uuid) {
    if (this.data.callbackHandlers[uuid] == null) {
      return;
    }
    if (this.data.callbackHandlers[uuid].complete != null) {
      this.data.callbackHandlers[uuid].complete();
    }
    delete this.data.callbackHandlers[uuid];
  },

  /**
   * 生成原生交互参数
   *
   * @param {*} object
   * @returns
   */
  createParams: function (object) {
    let uuid = this.uuid();
    this.data.callbackHandlers[uuid] = object;

    let params = {
      uuid: uuid,
      ...object,
    };
    params["success"] = "app.callbackSuccess";
    params["fail"] = "app.callbackFail";
    params["complete"] = "app.callbackComplete";

    return params;
  },

  /**
   * 发起 HTTP 网络请求
   *
   * @param {*} object
   */
  request: function (object) {
    this.postMessage("request", this.createParams(object));
  },

  /**
   * 关闭弹窗
   *
   * @param {*} object
   */
  closeDialog: function (object) {
    this.postMessage("closeDialog", this.createParams(object));
  },
};
