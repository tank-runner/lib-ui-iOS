//
//  BaseNavigationViewController.swift
//  
//
//  Created by 孙长坦 on 2022/1/2.
//

import Foundation
import UIKit

open class BaseNavigationViewController: UINavigationController {
    public override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        
        delegate = self
    }
    
    public override init(rootViewController: UIViewController) {
        super.init(rootViewController: rootViewController)
        
        delegate = self
    }
    
    required public init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension BaseNavigationViewController: UINavigationControllerDelegate {
    public func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if operation == .push, let transitonType = (toVC as? TransitionProtocol)?.transitionType {
            return TransitionManager.shared.pushClass(type: transitonType)?.init()
        } else if operation == .pop, let transitonType = (fromVC as? TransitionProtocol)?.transitionType {
            return TransitionManager.shared.popClass(type: transitonType)?.init()
        } else {
            return nil
        }
    }
}
