//
//  IconFonts.swift
//  
//
//  Created by tank on 2021/12/16.
//

import Foundation
import LibBase
import UIKit

public protocol IconFontsDataSource: AnyObject {
    var fontPath: String { get }
    
    var iconFonts: [String: String] { get }
    
    var iconNavigationBack: String { get }
}

public class IconFonts {

    public static let shared = IconFonts()

    public static weak var dataSource: IconFontsDataSource?

    private init() {
        guard let path = IconFonts.dataSource?.fontPath else {
            return
        }

        _ = UIFont.ex.registerFont(forResource: path, bundle: Bundle.main)
    }

    func iconFont(size: CGFloat) -> UIFont {
        return UIFont(name: "iconfont", size: size) ?? UIFont.systemFont(ofSize: size)
    }
}

extension IconFonts: ExtensionCompatible {
    
}
