//
//  WebViewService.swift
//  
//
//  Created by 孙长坦 on 2022/4/22.
//

import Foundation
import Logging
import Moya
import RxSwift
import LibNetwork

public class WebViewService {
    
    public static let shared = WebViewService()
    
    var logger = Logger(label: "LibUI.WebViewService")
    
    let moyaProvider: MoyaProvider<WebViewAPI>
    
    private init() {
        logger.logLevel = .debug
        moyaProvider = HttpService.shared.moyaProvider()
    }
    
    public func request(baseUrl: String?,
                 url: String,
                 method: String?,
                 headers: [String: String]?,
                 params: [String: Any]?,
                 data: Any?) -> Single<String>{
        return moyaProvider
            .rx
            .requestEx(.request(baseUrl: baseUrl,
                                url: url,
                                method: method,
                                headers: headers,
                                params: params,
                                data: data))
            .mapString()
    }
    
    public func download(url: URL, fileName: String?) -> Observable<ProgressResponse> {
        return moyaProvider
            .rx
            .requestWithProgressEx(.download(url: url, fileName: fileName))
    }
}
