//
//  WebViewMessageManager.swift
//  
//
//  Created by wuxianda on 2023/9/7.
//

import Foundation
import UIKit

public protocol WebViewMessageManagerDelegate: AnyObject {
    func onWebViewMessage(messageId: String, params: [String: Any])
}

public class WebViewMessageManager {
    static public let shared = WebViewMessageManager()
    
    public weak var delegate: WebViewMessageManagerDelegate?
    
    private init(){
        
    }
    
    public func onWebViewMessage(messageId: String, params: [String: Any]) {
        delegate?.onWebViewMessage(messageId: messageId, params: params)
    }
}
