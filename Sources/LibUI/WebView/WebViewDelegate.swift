//
//  WebViewDelegate.swift
//  
//
//  Created by 孙长坦 on 2022/6/9.
//

import Foundation
import WebKit

public protocol WebViewDelegate: AnyObject {
    func webView(_ webView: WKWebViewEx,
                 decidePolicyFor navigationAction: WKNavigationAction,
                 decisionHandler: @escaping (WKNavigationActionPolicy) -> Void)
    
    func webView(_ webView: WKWebViewEx,
                 decidePolicyFor navigationResponse: WKNavigationResponse,
                 decisionHandler: @escaping (WKNavigationResponsePolicy) -> Void)

    func webView(_ webView: WKWebViewEx, didFinish navigation: WKNavigation!)
    
    func webView(_ webView: WKWebViewEx,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error)
    
    func webView(_ webView: WKWebViewEx,
                 didFail navigation: WKNavigation!,
                 withError error: Error)
    
    func webViewDidClose(_ webView: WKWebViewEx)
    
    func webView(_ webView: WKWebViewEx, setRightMenus: [MenuData]?)
}

public extension WebViewDelegate {
    func webView(_ webView: WKWebViewEx, didFinish navigation: WKNavigation!) {
        
    }
    
    func webView(_ webView: WKWebViewEx,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        
    }
    
    func webView(_ webView: WKWebViewEx,
                 didFail navigation: WKNavigation!,
                 withError error: Error) {
        
    }
}
