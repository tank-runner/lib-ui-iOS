//
//  WebViewUIDelegate.swift
//  
//
//  Created by 孙长坦 on 2022/11/11.
//

import Foundation
import WebKit

public protocol WebViewUIDelegate: AnyObject {
    func webView(_ webView: WKWebViewEx,
                 createWebViewWith configuration: WKWebViewConfiguration,
                 for navigationAction: WKNavigationAction,
                 windowFeatures: WKWindowFeatures) -> WKWebView?
    
}
