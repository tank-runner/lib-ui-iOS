//
//  WebViewHandlerProtocool.swift
//  
//
//  Created by tank on 2021/12/4.
//

import Foundation
import WebKit

protocol WebViewHandlerProtocol: AnyObject {
    
    static var id: String { get }
    
    func handler(_ handler: WebViewHandlerProtocol,
                 webView: WKWebViewEx?,
                 frameInfo: WKFrameInfo,
                 params: [String: Any]?)
}

extension WebViewHandlerProtocol {
    
    func runSuccessCallback(webView: WKWebViewEx,
                            uuid: String,
                            code: Int,
                            message: String,
                            data: String?,
                            handlerParams: [String: Any],
                            completion: ((Any?, Error?) -> Void)? = nil) {
        runCallback(webView: webView,
                    callbackMethodName: "success",
                    uuid: uuid,
                    code: code,
                    message: message,
                    data: data,
                    handlerParams: handlerParams,
                    completion: completion)
        
        runCompleteCallback(webView: webView, uuid: uuid, handlerParams: handlerParams)
    }
    
    func runFailCallback(webView: WKWebViewEx,
                         uuid: String,
                         code: Int,
                         message: String,
                         data: String? = nil,
                         handlerParams: [String: Any],
                         completion: ((Any?, Error?) -> Void)? = nil) {
        runCallback(webView: webView,
                    callbackMethodName: "fail",
                    uuid: uuid,
                    code: code,
                    message: message,
                    handlerParams: handlerParams,
                    completion: completion)
        
        runCompleteCallback(webView: webView, uuid: uuid, handlerParams: handlerParams)
    }
    
    private func runCompleteCallback(webView: WKWebViewEx,
                                     uuid: String,
                                     handlerParams: [String: Any],
                                     completion: ((Any?, Error?) -> Void)? = nil) {
        guard let callbackMethodName = handlerParams["complete"] as? String else {
            completion?(false, nil)
            return
        }
        
        let js: String = String(format: "%@('%@')", callbackMethodName, uuid)
        webView.evaluateJavaScript(js, completionHandler: completion)
    }
    
    
    private func runCallback(webView: WKWebViewEx,
                             callbackMethodName: String,
                             uuid: String,
                             code: Int,
                             message: String,
                             data: String? = nil,
                             handlerParams: [String: Any],
                             completion: ((Any?, Error?) -> Void)? = nil) {
        guard let callbackMethodName = handlerParams[callbackMethodName] as? String else {
            completion?(false, nil)
            return
        }
        
        let js: String = String(format: "%@('%@', %d, '%@', %@)", callbackMethodName, uuid, code, message, data ?? "null")
        webView.evaluateJavaScript(js, completionHandler: completion)
    }
}
