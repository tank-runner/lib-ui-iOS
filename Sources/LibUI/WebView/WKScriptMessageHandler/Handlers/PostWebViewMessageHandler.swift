//
//  PostWebViewMessageHandler.swift
//  
//
//  Created by wuxianda on 2023/9/6.
//

import Foundation
import WebKit

class PostWebViewMessageHandler: WebViewHandlerProtocol {
    static var id: String = "postWebViewMessage"
    
    func handler(_ handler: WebViewHandlerProtocol,
                 webView: WKWebViewEx?,
                 frameInfo: WKFrameInfo,
                 params: [String: Any]?) {
        guard let webView = webView else {
            return
        }
        
        guard let params = params,
              let uuid = params["uuid"] as? String else {
            return
        }
        
        if let messageId = params["messageId"] as? String, let data = params["data"] as? [String: Any] {
            WebViewMessageManager.shared.onWebViewMessage(messageId: messageId, params: data)
        }
        
        runSuccessCallback(webView: webView,
                           uuid: uuid, code: 0, message: "postWebViewMessage:ok",
                           data: nil, handlerParams: params)
    }
}
