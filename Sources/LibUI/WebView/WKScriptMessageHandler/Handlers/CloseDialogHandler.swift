//
//  CloseDialogHandler.swift
//  
//
//  Created by tank on 2021/12/4.
//

import Foundation
import WebKit

class CloseDialogHandler: WebViewHandlerProtocol {

    static let id: String = "closeDialog"

    func handler(_ handler: WebViewHandlerProtocol,
                 webView: WKWebViewEx?,
                 frameInfo: WKFrameInfo,
                 params: [String: Any]?) {

        guard let webView = webView else {
            return
        }

        guard let params = params,
              let uuid = params["uuid"] as? String else {
            return
        }

        guard let dialogId = params["dialogId"] as? String else {
            runFailCallback(webView: webView,
                        uuid: uuid,
                        code: 1,
                        message: "closeDialog:fail, no dialogId",
                        handlerParams: params)
            return
        }

        guard let buttonId = params["buttonId"] as? String else {
            runFailCallback(webView: webView,
                        uuid: uuid,
                        code: 2,
                        message: "closeDialog:fail, no buttonId",
                        handlerParams: params)
            return
        }

        Dialog.shared.closeDialog(dialogId: dialogId, buttonId: buttonId)

        runSuccessCallback(webView: webView,
                    uuid: uuid,
                    code: 0,
                    message: "closeDialog:ok",
                    data: nil,
                    handlerParams: params)
    }
}
