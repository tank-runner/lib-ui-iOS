//
//  ColorWrapper.swift
//
//
//  Created by tank on 2021/12/14.
//

import Foundation
import UIKit

@propertyWrapper public struct ColorWrapper: Codable {

    public var wrappedValue: UIColor?

    public init(wrappedValue: UIColor?) {
        self.wrappedValue = wrappedValue
    }

    public init(from decoder: Decoder) throws {
        let container = try decoder.singleValueContainer()
        
        if let hexValue = try? container.decode(String.self) {
            wrappedValue = UIColor.ex.color(hex: hexValue)
        }
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.singleValueContainer()
        
        if let wrappedValue = wrappedValue {
            try container.encode(wrappedValue.ex.hexARGB)
        }
    }
}
