//
//  TabViewDataSource.swift
//  
//
//  Created by 孙长坦 on 2022/4/28.
//

import Foundation
import SwiftUI

public protocol TabViewDataSource: AnyObject {
    func numberOfItems(in tabView: TabView) -> Int
    
    func tabView(_ tabView: TabView, itemForIndex: Int) -> UIView
}
