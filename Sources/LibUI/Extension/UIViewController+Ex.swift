//
//  UIViewController+Ex.swift
//  
//
//  Created by 孙长坦 on 2022/4/26.
//

import Foundation
import UIKit
import LibBase

extension Extension where Base == UIViewController {
    static var rootViewController: UIViewController? {
        let currentWindows: [UIWindow]
        
        if let keyWindow = UIApplication.shared.keyWindow {
            currentWindows = [keyWindow]
        } else {
            currentWindows = UIApplication.shared.windows
        }
        
        guard currentWindows.count > 0 else {
            return nil
        }
        
        var rootViewController: UIViewController?
        for window in currentWindows {
            if let windowRootViewController = window.rootViewController {
                rootViewController = windowRootViewController
                break
            }
        }
        
        return rootViewController
    }
    
    /// Returns the current application's top most view controller.
    static var topMost: UIViewController? {
        return self.topMost(of: rootViewController)
    }
    
    /// Returns the top most view controller from given view controller's stack.
    static func topMost(of viewController: UIViewController?) -> UIViewController? {
        // presented view controller
        if let presentedViewController = viewController?.presentedViewController {
            return self.topMost(of: presentedViewController)
        }
        
        // UITabBarController
        if let tabBarController = viewController as? UITabBarController,
           let selectedViewController = tabBarController.selectedViewController {
            return self.topMost(of: selectedViewController)
        }
        
        // UINavigationController
        if let navigationController = viewController as? UINavigationController,
           let visibleViewController = navigationController.visibleViewController {
            return self.topMost(of: visibleViewController)
        }
        
        // UIPageController
        if let pageViewController = viewController as? UIPageViewController,
           pageViewController.viewControllers?.count == 1 {
            return self.topMost(of: pageViewController.viewControllers?.first)
        }
        
        // child view controller
        for subview in viewController?.view?.subviews ?? [] {
            if let childViewController = subview.next as? UIViewController {
                return self.topMost(of: childViewController)
            }
        }
        
        return viewController
    }
}
