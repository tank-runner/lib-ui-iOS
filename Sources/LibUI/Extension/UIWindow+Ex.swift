//
//  UIWindow+Ex.swift
//  
//
//  Created by tank on 2021/11/15.
//

import Foundation
import UIKit
import LibBase

extension Extension where Base == UIWindow {
    public static var key: UIWindow? {
        if #available(iOS 13, *) {
            return UIApplication.shared.connectedScenes
                .filter({$0.activationState == .foregroundActive})
                .compactMap({$0 as? UIWindowScene})
                .first?.windows
                .filter({$0.isKeyWindow}).first
        } else {
            return UIApplication.shared.keyWindow
        }
    }
}
