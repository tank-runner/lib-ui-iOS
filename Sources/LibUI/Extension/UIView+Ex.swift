//
//  UIView+Ex.swift
//  
//
//  Created by tank on 2021/12/16.
//

import Foundation
import LibBase
import UIKit

extension Extension where Base: UIView {
    
    public var viewController: UIViewController? {
        if let nextResponder = base.next as? UIViewController {
            return nextResponder
        } else if let nextResponder = base.next as? UIView {
            return nextResponder.ex.viewController
        } else {
            return nil
        }
    }
    
    func addTopBorder(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        base.addSubview(border)
        border.snp.makeConstraints { make in
            make.top.left.right.equalToSuperview()
            make.height.equalTo(width)
        }
    }
    
    func addRightBorder(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        base.addSubview(border)
        border.snp.makeConstraints { make in
            make.right.top.bottom.equalToSuperview()
            make.width.equalTo(width)
        }
    }
    
    func addBottomBorder(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        base.addSubview(border)
        border.snp.makeConstraints { make in
            make.left.right.bottom.equalToSuperview()
            make.height.equalTo(width)
        }
    }
    
    func addLeftBorder(color: UIColor, width: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        base.addSubview(border)
        border.snp.makeConstraints { make in
            make.left.top.bottom.equalToSuperview()
            make.width.equalTo(width)
        }
    }
}
