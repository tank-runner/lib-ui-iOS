//
//  String+Ex.swift
//
//
//  Created by tank on 6/17/24.
//

import Foundation
import UIKit
import LibBase

extension Extension where Base == String {

    public var color: UIColor? {
        return UIColor.ex.color(hex: base)
    }
}
