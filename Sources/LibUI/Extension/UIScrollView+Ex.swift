//
//  UIScrollView+Ex.swift
//  
//
//  Created by 孙长坦 on 2022/7/9.
//

import Foundation
import UIKit
import LibBase

extension Extension where Base == UIScrollView {
    public func scrollRectToVisibleCenteredOn(_ visibleRect: CGRect, animated: Bool) {
        let centeredRect = CGRect(x: visibleRect.origin.x + visibleRect.width / 2 - base.frame.width / 2,
                                  y: visibleRect.origin.y + visibleRect.height / 2 - base.frame.height / 2,
                                  width: base.frame.width,
                                  height: base.frame.height)
        
        base.scrollRectToVisible(centeredRect, animated: animated)
    }
}
