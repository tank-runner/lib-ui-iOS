//
//  TabBarItem.swift
//  
//
//  Created by 孙长坦 on 2022/4/27.
//

import Foundation

public struct TabBarItem {
    public let title: String?
    
    public init(title: String?) {
        self.title = title
    }
}
