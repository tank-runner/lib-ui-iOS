//
//  LineIndicator.swift
//  
//
//  Created by 孙长坦 on 2022/7/21.
//

import Foundation
import UIKit
import QuartzCore

public class LineIndicator: CALayer, IndicatorProtocol {
    public var indicatorHeight: CGFloat = 2
    public var padding: UIEdgeInsets = UIEdgeInsets(top: 0, left: -24, bottom: 3, right: -24)
    public var indicatorMinWidth: CGFloat = 20
   
    public override init(layer: Any) {
        super.init(layer: layer)
    }
    
    public override init() {
        super.init()
        
        masksToBounds = true
        cornerRadius = 3
        
        backgroundColor = Theme.shared.currentThemeData.topTabBarIndicatorColor.cgColor
        
        bounds = CGRect(x: 0, y: 0,
                        width: 0, height: indicatorHeight)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public func set(frame: CGRect, animated: Bool) {
        CATransaction.begin()
        CATransaction.setDisableActions(!animated)
        
        self.frame = frame.insetBy(dx: 0, dy: (frame.height - indicatorHeight) / 2)
        
        CATransaction.commit()
    }
    
    public func moveTo(item: UIView, animated: Bool) -> CGRect {
        let itemSize = item.sizeThatFits(CGSize(width: UIView.noIntrinsicMetric,
                                                height: UIView.noIntrinsicMetric))
        let itemFrame = item.frame
        
        var indicatorFrame = CGRect(origin: .zero, size: itemSize)
        indicatorFrame.size.width = indicatorFrame.width + padding.left + padding.right
        if indicatorFrame.width < indicatorMinWidth {
            indicatorFrame.size.width = indicatorMinWidth
        }
        indicatorFrame.origin.x = itemFrame.midX - indicatorFrame.width / 2
        indicatorFrame.origin.y = itemFrame.maxY - padding.bottom - indicatorHeight
        indicatorFrame.size.height = indicatorHeight
        set(frame: indicatorFrame, animated: animated)
        
        return indicatorFrame
    }
    
    public func onMove(fromItem: UIView, toItem: UIView, movePrecent: CGFloat) -> CGRect {
        let fromSize = fromItem.sizeThatFits(CGSize(width: UIView.noIntrinsicMetric,
                                                    height: UIView.noIntrinsicMetric))
        
        let toSize = toItem.sizeThatFits(CGSize(width: UIView.noIntrinsicMetric,
                                                height: UIView.noIntrinsicMetric))
        
        let fromFrame = fromItem.frame.insetBy(dx: (fromItem.frame.width - fromSize.width) / 2, dy: 0)
        let toFrame = toItem.frame.insetBy(dx: (toItem.frame.width - toSize.width) / 2, dy: 0)
        let start = fromFrame.midX
        let end = toFrame.midX
        
        let dx = (toFrame.width - fromFrame.width) * abs(movePrecent)
        let midX = start + abs(end - start) * movePrecent
        
        var indicatorFrame = fromFrame.insetBy(dx: -(dx + padding.left + padding.right) / 2, dy: 0)
        if indicatorFrame.width < indicatorMinWidth {
            indicatorFrame.size.width = indicatorMinWidth
        }
        indicatorFrame.origin.x = midX - indicatorFrame.width / 2
        indicatorFrame.origin.y = toFrame.maxY - padding.bottom - indicatorHeight
        indicatorFrame.size.height = indicatorHeight
        
        set(frame: indicatorFrame, animated: false)
        
        return indicatorFrame
    }
}
