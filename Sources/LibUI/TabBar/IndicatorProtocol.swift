//
//  IndicatorProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/7/21.
//

import Foundation
import UIKit

public protocol IndicatorProtocol: CALayer {
    var indicatorHeight: CGFloat { get set }
    var padding: UIEdgeInsets { get set }
    
    func set(frame: CGRect, animated: Bool)
    
    @discardableResult
    func moveTo(item: UIView, animated: Bool) -> CGRect
    
    func onMove(fromItem: UIView, toItem: UIView, movePrecent: CGFloat) -> CGRect
}
