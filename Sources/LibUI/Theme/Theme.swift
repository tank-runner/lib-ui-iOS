//
//  Theme.swift
//  
//
//  Created by 孙长坦 on 2022/5/25.
//

import Foundation
import UIKit

public class Theme {
    public private(set) static var shared: Theme!
    
    public static func initTheme(lightThemeData: ThemeData, darkThemeData: ThemeData? = nil) {
        shared = Theme(lightThemeData: lightThemeData, darkThemeData: darkThemeData)
    }
    
    public var lightThemeData: ThemeData
    
    public var darkThemeData: ThemeData?
    
    public var currentThemeData: ThemeData {
        if #available(iOS 13.0, *) {
            return UITraitCollection.current.userInterfaceStyle == .dark ?
            (darkThemeData ?? lightThemeData) : lightThemeData
        } else {
            return lightThemeData
        }
    }
    
    private init(lightThemeData: ThemeData, darkThemeData: ThemeData?) {
        self.lightThemeData = lightThemeData
        self.darkThemeData = darkThemeData
    }
}
