//
//  ThemeData.swift
//  
//
//  Created by 孙长坦 on 2022/5/25.
//

import Foundation
import UIKit

public struct ThemeData {
    /// 主题颜色
    public let primaryColor: UIColor
    
    /// 导航栏颜色
    public let navigationBarColor: UIColor
    
    /// 导航栏标题、按钮颜色
    public let navigationBarTintColor: UIColor
    
    /// 底部 tab bar 字体颜色
    public let tabBarTextColor: UIColor
    
    /// 底部 tab bar 图标颜色
    public let tabBarIconColor: UIColor
    
    /// 顶部 tab bar 背景色
    public let topTabBarBackgroundColor: UIColor
    
    /// 顶部 tab bar 字体颜色
    public let topTabBarTextColor: UIColor
    
    public let topTabBarSelectedTextColor: UIColor
   
    /// 顶部 tab bar indicator 颜色
    public let topTabBarIndicatorColor: UIColor
    
    /// 顶部 tab bar bottom border 颜色
    public let topTabBarBottomBorderColor: UIColor
    
    public let topTabBarBottomWidth: CGFloat
    
    public init(primaryColor: UIColor,
                navigationBarColor: UIColor,
                navigationBarTintColor: UIColor,
                tabBarTextColor: UIColor,
                tabBarIconColor: UIColor,
                topTabBarBackgroundColor: UIColor,
                topTabBarTextColor: UIColor,
                topTabBarSelectedTextColor: UIColor,
                topTabBarIndicatorColor: UIColor,
                topTabBarBottomBorderColor: UIColor,
                topTabBarBottomWidth: CGFloat) {
        self.primaryColor = primaryColor
        
        self.navigationBarColor = navigationBarColor
        
        self.navigationBarTintColor = navigationBarTintColor
        
        self.tabBarTextColor = tabBarTextColor
        
        self.tabBarIconColor = tabBarIconColor
        
        self.topTabBarBackgroundColor = topTabBarBackgroundColor
    
        self.topTabBarTextColor = topTabBarTextColor

        self.topTabBarSelectedTextColor = topTabBarSelectedTextColor

        self.topTabBarIndicatorColor = topTabBarIndicatorColor
        
        self.topTabBarBottomBorderColor = topTabBarBottomBorderColor
        
        self.topTabBarBottomWidth = topTabBarBottomWidth
    }
}
