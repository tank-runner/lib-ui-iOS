//
//  TransitionProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/1/2.
//

import Foundation

public protocol TransitionProtocol {
    
    var transitionType: String? { get }
}
