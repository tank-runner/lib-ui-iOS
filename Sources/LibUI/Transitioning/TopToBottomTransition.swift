//
//  TopToBottomTransition.swift
//  
//
//  Created by 孙长坦 on 2022/1/2.
//

import Foundation
import UIKit

public class TopToBottomTransition: NSObject, AnimatedTransitioning {
    
    required public override init() {
        super.init()
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.3
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toView = transitionContext.view(forKey: .to)
        let fromView = transitionContext.view(forKey: .from)

        transitionContext.containerView.addSubview(toView!)
        transitionContext.containerView.addSubview(fromView!)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, options: [.curveEaseIn], animations: {
            fromView?.frame.origin.y = fromView?.frame.height ?? 0
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
}
