//
//  TransitionManager.swift
//  
//
//  Created by 孙长坦 on 2022/1/2.
//

import Foundation
import UIKit

public protocol AnimatedTransitioning: UIViewControllerAnimatedTransitioning {
    
    init()
}

public struct TransitionData {
    
    public let type: String
    
    public let pushClass: AnimatedTransitioning.Type
    
    public let popClass: AnimatedTransitioning.Type
}

public class TransitionManager {
    
    public static let shared = TransitionManager()
    
    public private(set) var transitionDatas: [TransitionData] = [
        TransitionData(type: "modal", pushClass: BottomToTopTransition.self, popClass: TopToBottomTransition.self)
    ]
    
    
    private init() {
        
    }
    
    public func pushClass(type: String) -> AnimatedTransitioning.Type? {
        return transitionDatas.first(where: { $0.type == type })?.pushClass
    }
    
    public func popClass(type: String) -> AnimatedTransitioning.Type? {
        return transitionDatas.first(where: { $0.type == type })?.popClass
    }
}
