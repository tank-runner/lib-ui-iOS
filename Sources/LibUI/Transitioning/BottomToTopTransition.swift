//
//  BottomToTopTransition.swift
//  
//
//  Created by 孙长坦 on 2022/1/2.
//

import Foundation
import UIKit

public class BottomToTopTransition: NSObject, AnimatedTransitioning {
    
    required public override init() {
        super.init()
    }
    
    public func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.2
    }

    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        let toView = transitionContext.view(forKey: .to)
        let fromView = transitionContext.view(forKey: .from)

        transitionContext.containerView.addSubview(fromView!)
        transitionContext.containerView.addSubview(toView!)
        
        toView?.frame.origin.y = toView?.frame.height ?? 0

        UIView.animate(withDuration: transitionDuration(using: transitionContext), delay: 0, options: [.curveEaseOut], animations: {
            toView?.frame.origin.y = 0
        }, completion: { _ in
            transitionContext.completeTransition(true)
        })
    }
}
