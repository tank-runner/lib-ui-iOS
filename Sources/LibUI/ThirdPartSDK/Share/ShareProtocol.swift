//
//  ShareProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public protocol ShareProtocol: AnyObject {
    typealias Completion = (_ success: Bool) -> Void
    
    static var type: String { get }
    
    static var title: String { get }
    
    static var icon: String? { get }
    
    static var selectedIcon: String? { get }
    
    var type: String { get }
    
    var title: String { get }
    
    var icon: String? { get }
    
    var selectedIcon: String? { get }
    
    // 接受分享的类
    var acceptObjectTypes: ShareObjectType { get }
    
    var isSelected: Bool { get set }
    
    
    init()
    
    func share(shareObject: ShareObjectProtocol, completion: Completion?)
    
    func shareUrl(shareObject: ShareUrlObject, completion: Completion?)
    
    func shareFile(shareObject: ShareFileObject, completion: Completion?)
    
    func shareImage(shareObject: ShareImageObject, completion: Completion?)
    
    func shareMiniApp(shareObject: ShareMiniAppObject, completion: Completion?)
    
    func shareCustomize(shareObject: ShareCustomizeObject, completion: Completion?)
    
    func addShareParams(url: String, completion: @escaping (_: String) -> Void)
}

public extension ShareProtocol {
    var type: String {
        return Self.type
    }
    
    var title: String {
        return Self.title
    }
    
    var icon: String? {
        return Self.icon
    }
    
    var selectedIcon: String? {
        return Self.selectedIcon
    }
    
    func share(shareObject: ShareObjectProtocol, completion: Completion?) {
        switch shareObject.type {
        case .url:
            guard var shareUrlObject = shareObject as? ShareUrlObject else {
                completion?(false)
                return
            }
            
            addShareParams(url: shareUrlObject.url) { url in
                shareUrlObject.url = url
                
                self.shareUrl(shareObject: shareUrlObject, completion: completion)
            }
            
        case .file:
            guard let shareFileObject = shareObject as? ShareFileObject else {
                completion?(false)
                return
            }
            shareFile(shareObject: shareFileObject, completion: completion)
            
        case .image:
            guard let shareImageObject = shareObject as? ShareImageObject else {
                completion?(false)
                return
            }
            shareImage(shareObject: shareImageObject, completion: completion)
            
        case .miniApp:
            guard let shareMiniAppObject = shareObject as? ShareMiniAppObject else {
                completion?(false)
                return
            }
            shareMiniApp(shareObject: shareMiniAppObject, completion: completion)
            
        case .customize:
            guard let shareCustomizeObject = shareObject as? ShareCustomizeObject else {
                completion?(false)
                return
            }
            shareCustomize(shareObject: shareCustomizeObject, completion: completion)
            
        default:
            completion?(false)
        }
    }
    
    func shareUrl(shareObject: ShareUrlObject, completion: Completion?) {
        completion?(false)
    }
    
    func shareFile(shareObject: ShareFileObject, completion: Completion?) {
        completion?(false)
    }
    
    func shareImage(shareObject: ShareImageObject, completion: Completion?) {
        completion?(false)
    }
    
    func shareMiniApp(shareObject: ShareMiniAppObject, completion: Completion?) {
        completion?(false)
    }
    
    func shareCustomize(shareObject: ShareCustomizeObject, completion: Completion?) {
        completion?(false)
    }
    
    func addShareParams(url: String, completion: @escaping (_: String) -> Void) {
        SDKManager.shared.delegate?.shareParams(completion: { shareParams in
            guard let shareParams = shareParams else {
                completion(url)
                return
            }
            
            completion(url.ex.add(urlParameters: shareParams))
        })
    }
}
