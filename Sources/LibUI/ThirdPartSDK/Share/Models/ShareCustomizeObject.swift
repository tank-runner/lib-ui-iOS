//
//  ShareCustomizeObject.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public struct ShareCustomizeObject: ShareObjectProtocol {
    public let type: ShareObjectType = .customize
    
    public let title: String?
    
    public let description: String?
    
    public let thumbImage: UIImage?
    
    public let params: [String: Any]?
    
    init(title: String? = nil,
         description: String? = nil,
         thumbImage: UIImage?,
         params: [String: Any]?) {
        
        self.title = title
        self.description = description
        self.thumbImage = thumbImage
        self.params = params
    }
    
    public func toJson() -> [String: Any] {
        return [
            "type": "url",
        ]
    }
}
