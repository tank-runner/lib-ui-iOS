//
//  ShareUrlObject.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public struct ShareUrlObject: ShareObjectProtocol {
    /// 分享类别
    public var type: ShareObjectType = .url
    
    /// 分享标题
    public let title: String?
    
    /// 分享描述
    public let description: String?
    
    /// 分享图标
    public let thumbImage: UIImage?
    
    /// 链接
    public var url: String
    
    public init(title: String? = nil,
                description: String? = nil,
                thumbImage: UIImage? = nil,
                url: String) {
        
        self.title = title
        self.description = description
        self.thumbImage = thumbImage
        self.url = url.ex.absoluteH5Url
    }
    
    public func toJson() -> [String: Any] {
        return [
            "type": "url",
        ]
    }
}
