//
//  ShareFileObject.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public struct ShareFileObject: ShareObjectProtocol {
    /// 分享类别
    public let type: ShareObjectType = .file
    
    /// 分享标题
    public let title: String?
    
    /// 分享描述
    public let description: String?
    
    /// 分享图标
    public let thumbImage: UIImage?
    
    /// 文件URL
    public let fileUrl: String?
    
    /// 文件内容
    public let fileData: Data?
    
    /// 文件名
    public let fileName: String?
    
    public init(title: String?,
                description: String?,
                thumbImage: UIImage? = nil,
                fileUrl: String?,
                fileData: Data?,
                fileName: String?) {
        self.title = title
        self.description = description
        self.thumbImage = thumbImage
        self.fileUrl = fileUrl
        self.fileData = fileData
        self.fileName = fileName
    }
    
    public func toJson() -> [String: Any] {
        return [
            "type": "url",
        ]
    }
}
