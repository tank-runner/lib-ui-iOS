//
//  ShareObjectType.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation

public struct ShareObjectType: OptionSet, Codable {
    public let rawValue: Int
    
    public static let url = ShareObjectType(rawValue: 1 << 0)
    public static let file = ShareObjectType(rawValue: 1 << 1)
    public static let image = ShareObjectType(rawValue: 1 << 2)
    public static let miniApp = ShareObjectType(rawValue: 1 << 3)
    public static let customize = ShareObjectType(rawValue: 1 << 4)
    
    public static let all: ShareObjectType = [.url, .file, .image, miniApp, customize]
    
    public static let allTypes: [String: ShareObjectType] = [
        "url": .url,
        "file": .file,
        "image": .image,
        "miniApp": .miniApp,
        "customize": .customize,
        "all": .all
    ]
    
    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
}
