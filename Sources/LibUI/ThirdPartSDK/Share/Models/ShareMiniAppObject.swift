//
//  ShareMiniAppObject.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

/// 小程序
public struct ShareMiniAppObject: ShareObjectProtocol {
    /// 分享类别
    public let type: ShareObjectType = .miniApp
    
    /// 分享标题
    public let title: String?

    /// 分享描述
    public let description: String?

    /// 分享图标
    public let thumbImage: UIImage?
    
    /// 小程序兼容网页链接
    public let webpageUrl: String?
    
    /// 小程序id
    public let miniAppId: String
    
    /// 小程序路径
    public let path: String?
    
    public init(title: String?,
                description: String?,
                thumbImage: UIImage?,
                webpageUrl: String?,
                miniAppId: String,
                path: String?) {

        self.title = title
        self.description = description
        self.thumbImage = thumbImage
        self.webpageUrl = webpageUrl
        self.miniAppId = miniAppId
        self.path = path
    }
    
    public func toJson() -> [String: Any] {
        return [
            "type": "url",
        ]
    }
}
