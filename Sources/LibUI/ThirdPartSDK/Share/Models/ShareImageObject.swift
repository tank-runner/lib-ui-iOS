//
//  ShareImageObject.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public struct ShareImageObject: ShareObjectProtocol {
    /// 分享类别
    public let type: ShareObjectType = .image
    
    /// 分享标题
    public let title: String?

    /// 分享描述
    public let description: String?

    /// 分享图标
    public let thumbImage: UIImage?

    /// 要分享的图片
    public let image: UIImage

    public init(title: String? = nil,
                description: String? = nil,
                thumbImage: UIImage? = nil,
                image: UIImage) {

        self.title = title
        self.description = description
        self.thumbImage = thumbImage
        self.image = image
    }
    
    public func toJson() -> [String: Any] {
        return [
            "type": "url",
        ]
    }
}
