//
//  ShareObjectProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

public protocol ShareObjectProtocol {
    var type: ShareObjectType { get }
    
    /// 分享标题
    var title: String? { get }
    
    /// 分享描述
    var description: String? { get }
    
    /// 分享图标
    var thumbImage: UIImage? { get }
    
    func toJson() -> [String: Any]
}

