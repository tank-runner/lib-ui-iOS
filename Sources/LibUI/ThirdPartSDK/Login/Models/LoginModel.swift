//
//  LoginModel.swift
//  
//
//  Created by 孙长坦 on 2022/9/28.
//

import Foundation

public struct LoginModel {
    public let code: String
    
    public let token: String?
    
    public let nickName: String?
    
    public let avatar: String?
    
    public init(code: String,
                token: String? = nil,
                nickName: String? = nil,
                avatar: String? = nil) {
        self.code = code
        self.token = token
        self.nickName = nickName
        self.avatar = avatar
    }
}
