//
//  LoginProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/9/28.
//

import Foundation
import UIKit

public protocol LoginProtocol: AnyObject {
    typealias Completion = (_ result: Bool, _ message: String?, _ loginModel: LoginModel?) -> Void
    
    static var type: String { get }
    
    var type: String { get }
    
    var name: String { get }
    
    var icon: UIImage? { get }
    
    init(sdk: SDKProtocol)
    
    func login(completion: Completion?)
}

extension LoginProtocol {
    public var type: String {
        return Self.type
    }
}
