//
//  SystemShare.swift
//  
//
//  Created by 孙长坦 on 2022/8/31.
//

import Foundation
import UIKit

class SystemShare: ShareProtocol {
    static var type: String = "system"
    
    static var title: String = "系统"
    
    static var icon: String?
    
    static var selectedIcon: String?
    
    var acceptObjectTypes: ShareObjectType = [.file]
    
    var isSelected: Bool = false
    
    
    required init() {
    }
    
    func shareUrl(shareObject: ShareUrlObject, completion: Completion?) {
        guard let title = shareObject.title,
              let url = URL(string: shareObject.url) else {
            completion?(false)
            return
        }
        
        let activity = UIActivityViewController(activityItems: [title, url], applicationActivities: nil)
        UIViewController.ex.topMost?.present(activity, animated: true)
        
        completion?(true)
    }
    
    func shareFile(shareObject: ShareFileObject, completion: Completion?) {
        guard let fileUrl = shareObject.fileUrl, let url = URL(string: fileUrl) else {
            completion?(false)
            return
        }
        
        let activity = UIActivityViewController(activityItems: [url], applicationActivities: nil)
        UIViewController.ex.topMost?.present(activity, animated: true)
        
        completion?(true)
    }
}

