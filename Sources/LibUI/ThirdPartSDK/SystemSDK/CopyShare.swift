//
//  CopyShare.swift
//  
//
//  Created by 孙长坦 on 2022/12/7.
//

import Foundation
import UIKit

class CopyShare: ShareProtocol {
    static var type: String = "copy"
    
    static var title: String = "复制链接"
    
    static var icon: String? = "copy.png"
    
    static var selectedIcon: String?
    
    var acceptObjectTypes: ShareObjectType = [.url]
    
    var isSelected: Bool = false
    
    
    required init() {
    }
    
    func shareUrl(shareObject: ShareUrlObject, completion: Completion?) {
        let pasteboard = UIPasteboard.general
        pasteboard.string = shareObject.url
        
        Toast.shared.toast(type: nil, message: "复制成功", interactive: true, duration: 100, completion: nil)
        
        completion?(true)
    }
}
