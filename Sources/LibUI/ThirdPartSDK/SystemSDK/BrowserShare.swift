//
//  BrowserShare.swift
//  
//
//  Created by 孙长坦 on 2022/12/7.
//

import Foundation
import UIKit

class BrowserShare: ShareProtocol {
    static var type: String = "browser"
    
    static var title: String = "浏览器"
    
    static var icon: String? = "browser.png"
    
    static var selectedIcon: String?
    
    var acceptObjectTypes: ShareObjectType = [.url]
    
    var isSelected: Bool = false
    
    
    required init() {
    }
    
    func shareUrl(shareObject: ShareUrlObject, completion: Completion?) {
        guard let url = URL(string: shareObject.url) else {
            completion?(false)
            return
        }
        
        UIApplication.shared.open(url)
        
        completion?(true)
    }
}
