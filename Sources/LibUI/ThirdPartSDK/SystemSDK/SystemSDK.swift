//
//  SystemSDK.swift
//  
//
//  Created by 孙长坦 on 2022/9/28.
//

import Foundation

class SystemSDK: SDKProtocol {
    private(set) static var type: String = "apple"
    
    private(set) static var sdkVersion: String?
    
    static var loginClass: LoginProtocol.Type? = SystemLogin.self
    
    static var shareClasses: [ShareProtocol.Type]? = [
        SystemShare.self,
        CopyShare.self,
        BrowserShare.self,
        AlbumShare.self,
    ]
    
    private(set) var isAppInstalled: Bool = true
    
    required init() {
        
    }
    
    func install() -> Bool {
        return true
    }
    
    func uninstall() {
    }
    
    func continueUserActivity(_ userActivity: NSUserActivity) -> Bool {
        return false
    }
    
    func handLinkURL(_ url: URL) -> Bool {
        return false
    }
}
