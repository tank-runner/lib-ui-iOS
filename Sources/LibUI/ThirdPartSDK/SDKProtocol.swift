//
//  SDKProtocol.swift
//  
//
//  Created by 孙长坦 on 2022/9/28.
//

import Foundation

public protocol SDKProtocol {
    static var type: String { get }
    
    static var sdkVersion: String? { get }

    static var loginClass: LoginProtocol.Type? { get }
    
    static var shareClasses: [ShareProtocol.Type]? { get }

    var type: String { get }
    
    var loginClass: LoginProtocol.Type? { get }
    
    var shareClasses: [ShareProtocol.Type]? { get }
    
    var isAppInstalled: Bool { get }
    
    var sdkVersion: String? { get }
    
    init()
    
    func install() -> Bool
    
    func uninstall()
    
    func handLinkURL(_ url: URL) -> Bool

    func continueUserActivity(_ userActivity: NSUserActivity) -> Bool
}

public extension SDKProtocol {
    var type: String {
        return Self.type
    }
    
    var sdkVersion: String? {
        return Self.sdkVersion
    }
    
    var loginClass: LoginProtocol.Type? {
        return Self.loginClass
    }
    
    var shareClasses: [ShareProtocol.Type]? {
        return Self.shareClasses
    }
}
