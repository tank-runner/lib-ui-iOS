//
//  LibUI.swift
//  
//
//  Created by 孙长坦 on 2022/6/9.
//

import Foundation

public protocol LibUIDataSource: AnyObject {
    var htmlPath: String { get }
    
    var lottiePath: String { get }
    
    var imagePath: String { get }
}

public class LibUI {
    static public let shared: LibUI = LibUI()
    
    public weak var dataSource: LibUIDataSource?
    
    public weak var webViewUIDeleagte: WebViewUIDelegate?
    
    private init() {
    }
}
