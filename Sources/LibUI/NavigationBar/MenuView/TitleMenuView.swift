//
//  TitleMenuView.swift
//  
//
//  Created by 孙长坦 on 2022/6/20.
//

import Foundation
import UIKit

public class TitleMenuView: UIButton {
    var menuData: MenuData?
    
    public var onMenuTap: ((_ menuData: MenuData?) -> Void)?
    
    override init(frame: CGRect = .zero) {
        super.init(frame: frame)
        
        setTitleColor(.white, for: .normal)
        titleLabel?.font = UIFont.systemFont(ofSize: 16)
        contentMode = .center
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        addGestureRecognizer(tap)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func createAttributedString(iconWithTag: String, menuData: MenuData) -> NSAttributedString? {
        let iconFontStr = String(iconWithTag.suffix(iconWithTag.count - NavigationBar.iconFontPrefix.count))
        
        guard let text = IconFonts.dataSource?.iconFonts[iconFontStr] else {
            return nil
        }
        
        return createAttributedString(text: text, menuData: menuData)
    }
    
    func createAttributedString(text: String, menuData: MenuData) -> NSAttributedString {
        return NSAttributedString(string: text,
                                  attributes: [.font: UIFont.ex.iconFont(ofSize: menuData.style?.fontData?.fontSize ?? 16),
                                               .foregroundColor: menuData.style?.color ?? tintColor ?? .white])
    }
    
    func setMenuData(menuData: MenuData) {
        self.menuData = menuData
        
        let attributedText = NSMutableAttributedString()
        
        if let icon = menuData.icon {
            if icon.hasPrefix(NavigationBar.iconFontPrefix) {
                if let attributedIcon = createAttributedString(iconWithTag: icon,
                                                               menuData: menuData) {
                    attributedText.append(attributedIcon)
                }
            } else {
                
            }
        }
        
        if let title = menuData.title {
            if attributedText.length > 0 {
                attributedText.append(NSAttributedString(string: " ", attributes: nil))
            }
            
            if title.hasPrefix(NavigationBar.iconFontPrefix) {
                if let attributedTitle = createAttributedString(iconWithTag: title,
                                                                menuData: menuData) {
                    attributedText.append(attributedTitle)
                }
            } else {
                attributedText
                    .append(createAttributedString(text: title, menuData: menuData))
            }
        }
        
        setAttributedTitle(attributedText, for: .normal)
        
        isEnabled = menuData.enabled
    }
    
    @objc func handleTap(_ sender: UITapGestureRecognizer) {
        if let children = menuData?.children, !children.isEmpty {
            let popMenuView = PopMenuView(frame: CGRect(x: 0, y: 0, width: 167, height: 50 * children.count),
                                          menuDatas: children)
            
            
            
            let popover = Popover(options: [
                .type(.down),
                .animationIn(0.3),
                .arrowSize(CGSize(width: 1, height: 0)),
                .cornerRadius(0),
                .sideEdge(0),
            ])
            
            let rootFrame = convert(bounds, to: nil)
            
            popover.show(popMenuView,
                         point: CGPoint(x: rootFrame.midX,
                                        y: rootFrame.maxY + (NavigationBar.height - NavigationBar.backButtonHeight) / 2))
            
            popMenuView.onMenuTap = { [weak self] menuData in
                popover.dismiss()
                
                self?.onMenuTap?(menuData)
            }
        }
        
        onMenuTap?(menuData)
    }
}
