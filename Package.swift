// swift-tools-version:5.5
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "LibUI",
    platforms: [
        .iOS(.v12)
    ],
    products: [
        // Products define the executables and libraries a package produces, and make them visible to other packages.
        .library(
            name: "LibUI",
            targets: ["LibUI"]),
    ],
    dependencies: [
        // Dependencies declare other packages that this package depends on.
        // .package(url: /* package url */, from: "1.0.0"),
        .package(name: "LibBase", path: "../LibBase"),
        .package(name: "LibNetwork", path: "../LibNetwork"),
        .package(name: "SnapKit", url: "git@gitee.com:tank-runner/SnapKit.git", .exact("5.0.1-appfixed.1")),
        .package(name: "SwiftMessages", url: "git@gitee.com:tank-runner/SwiftMessages.git", .exact("9.0.6-appfixed.1")),
        .package(name: "Lottie", url: "git@gitee.com:tank-runner/lottie-ios.git", .upToNextMajor(from: "3.4.0"))
    ],
    targets: [
        // Targets are the basic building blocks of a package. A target can define a module or a test suite.
        // Targets can depend on other targets in this package, and on products in packages this package depends on.
        .target(
            name: "LibUI",
            dependencies: ["LibBase",
                           "LibNetwork",
                           "SnapKit",
                           "SwiftMessages",
                           "Lottie",
                          ],
            path: "Sources",
            resources: [
                .process("LibUI/Resources.bundle"),
            ]),
        .testTarget(
            name: "LibUITests",
            dependencies: ["LibUI"]),
    ]
)
